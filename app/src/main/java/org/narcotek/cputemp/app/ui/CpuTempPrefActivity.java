package org.narcotek.cputemp.app.ui;

import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.github.danielnilsson9.colorpickerview.dialog.ColorPickerDialogFragment;
import com.github.danielnilsson9.colorpickerview.dialog.ColorPickerDialogFragment.ColorPickerDialogListener;
import com.github.danielnilsson9.colorpickerview.preference.ColorPreference;

import org.narcotek.cputemp.R;
import org.narcotek.cputemp.common.helper.IntentStrings;

import java.util.HashMap;

/**
 * Standard preference activity
 */
public final class CpuTempPrefActivity extends PreferenceActivity implements ColorPickerDialogListener {

    // ID for the color picker dialog
    private static final int FONT_COLOR_DIALOG_ID = 0;
    private static final int LIMIT_FONT_COLOR_DIALOG_ID = 1;

    // Necessary for dialogs
    private static final String FONT_COLOR_DIALOG_TAG = "font_color_dialog";
    private static final String LIMIT_FONT_COLOR_DIALOG_TAG = "limit_font_color_dialog";
    private static final String ABOUT_DIALOG_TAG = "about_dialog";
    private static final String LICENSES_DIALOG_TAG = "licenses_dialog";

    // Reference to preference fragment for dialog handling
    private CpuTempPrefFragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentFragment = new CpuTempPrefFragment();
        getFragmentManager().beginTransaction().replace(android.R.id.content, currentFragment).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_about:
                (new AboutDialog()).show(getFragmentManager(), ABOUT_DIALOG_TAG);
                return true;
            case R.id.menu_licenses:
                (new LicensesDialog()).show(getFragmentManager(), LICENSES_DIALOG_TAG);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onColorSelected(int dialogId, int color) {
        switch (dialogId) {
            case FONT_COLOR_DIALOG_ID:
                currentFragment.onColorSelected(dialogId, color);
                break;
            case LIMIT_FONT_COLOR_DIALOG_ID:
                currentFragment.onColorSelected(dialogId, color);
                break;
        }
    }

    @Override
    public void onDialogDismissed(int dialogId) {
        switch (dialogId) {
            case FONT_COLOR_DIALOG_ID:
                currentFragment.onDialogDismissed(dialogId);
                break;
            case LIMIT_FONT_COLOR_DIALOG_ID:
                currentFragment.onDialogDismissed(dialogId);
                break;
        }
    }

    public static class CpuTempPrefFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener, ColorPickerDialogListener {

        private ColorPreference fontColorPref;
        private ColorPreference limitFontColorPref;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.prefs);

            // Retrieving the font color preference
            fontColorPref = (ColorPreference) findPreference(getString(R.string.pref_key_font_color));

            // Setting a color picker dialog to show when the font color preference is clicked
            fontColorPref.setOnShowDialogListener(new ColorPreference.OnShowDialogListener() {
                @Override
                public void onShowColorPickerDialog(String title, int currentColor) {
                    ColorPickerDialogFragment dialog = ColorPickerDialogFragment
                            .newInstance(FONT_COLOR_DIALOG_ID,
                                    getString(R.string.pref_dialog_title_font_color),
                                    null,
                                    currentColor,
                                    false);
                    dialog.setStyle(DialogFragment.STYLE_NORMAL, R.style.ColorDialogTheme);
                    dialog.show(getFragmentManager(), FONT_COLOR_DIALOG_TAG);
                }
            });

            limitFontColorPref = (ColorPreference) findPreference(getString(R.string.pref_key_limit_font_color));

            // Setting a color picker dialog to show when the font color preference is clicked
            limitFontColorPref.setOnShowDialogListener(new ColorPreference.OnShowDialogListener() {
                @Override
                public void onShowColorPickerDialog(String title, int currentColor) {
                    ColorPickerDialogFragment dialog = ColorPickerDialogFragment
                            .newInstance(LIMIT_FONT_COLOR_DIALOG_ID,
                                    getString(R.string.pref_dialog_title_limit_font_color),
                                    null,
                                    currentColor,
                                    false);
                    dialog.setStyle(DialogFragment.STYLE_NORMAL, R.style.ColorDialogTheme);
                    dialog.show(getFragmentManager(), LIMIT_FONT_COLOR_DIALOG_TAG);
                }
            });
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            broadcastUpdatedPreferences();
        }

        @Override
        public void onColorSelected(int dialogId, int color) {
            switch (dialogId) {
                // Saving the new colors
                case FONT_COLOR_DIALOG_ID:
                    fontColorPref.saveValue(color);
                    broadcastUpdatedPreferences();
                    break;
                case LIMIT_FONT_COLOR_DIALOG_ID:
                    limitFontColorPref.saveValue(color);
                    broadcastUpdatedPreferences();
                    break;
            }
        }

        @Override
        public void onDialogDismissed(int dialogId) {
            // Dialog dismissed? Nothing to do.
        }

        private void broadcastUpdatedPreferences() {
            // If a preference was updated a intent is broadcasted to notify the custom TextView
            Intent i = new Intent();
            i.setAction(IntentStrings.INTENT_CPUTEMP_PREF_UPDATE);
            i.putExtra("prefs", new HashMap(this.getPreferenceManager().getSharedPreferences().getAll()));

            getActivity().sendBroadcast(i);
        }
    }

}
