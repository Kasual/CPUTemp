package org.narcotek.cputemp.app.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import org.narcotek.cputemp.common.helper.IntentStrings;

import java.util.HashMap;

/**
 * Small helper class which receives the BOOT_COMPLETED broadcast to transmit the saved settings to
 * the custom TextView in the SystemUI app.
 */
public final class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        SharedPreferences sp = context.getSharedPreferences("org.narcotek.cputemp_preferences", Context.MODE_PRIVATE);
        HashMap map = new HashMap(sp.getAll());

        Intent i = new Intent();
        i.setAction(IntentStrings.INTENT_CPUTEMP_PREF_UPDATE);
        i.putExtra("prefs", map);

        context.sendBroadcast(i);
    }

}
