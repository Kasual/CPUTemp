package org.narcotek.cputemp.systemui.io;

import java.io.File;

/**
 * An interface for a temperature reader. This interface serves to allow different ways of
 * retrieving the temperature from a temperature file.
 */
public interface TempReader {

    double readTemperature(File tempFile) throws Exception;

}
