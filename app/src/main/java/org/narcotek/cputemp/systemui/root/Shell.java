package org.narcotek.cputemp.systemui.root;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Small helper class representing a root shell. Allows running shell commands and getting their
 * results.
 */
public class Shell {

    private Process process;
    private BufferedReader input;
    private BufferedWriter output;

    /**
     * Default constructor allowing to choose between a root shell and a non-root shell.
     *
     * @param useRoot Ask for root access?
     * @throws IOException             If the binaries "su" or "sh" are not found
     * @throws RootNotGrantedException If root access was denied
     */
    public Shell(boolean useRoot) throws IOException, RootNotGrantedException {
        process = Runtime.getRuntime().exec(useRoot ? "su" : "sh");

        input = new BufferedReader(new InputStreamReader(process.getInputStream()));
        output = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));

        if (useRoot) {
            String id = run("id");
            if (id == null || !id.contains("uid=0")) {
                throw new RootNotGrantedException("Root not granted!");
            }
        }
    }

    /**
     * Runs the given command and returns the output.
     *
     * @param cmd Command to run
     * @return Command output
     * @throws IOException Command (binary/script) was not found
     */
    public String run(String cmd) throws IOException {
        output.write(cmd + "\n");
        output.flush();

        StringBuilder stringBuilder = new StringBuilder();
        while (true) {
            stringBuilder.append(input.readLine());
            if (!input.ready()) {
                break;
            }
        }

        return stringBuilder.toString();
    }

}
