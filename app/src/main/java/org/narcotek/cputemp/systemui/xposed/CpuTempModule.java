package org.narcotek.cputemp.systemui.xposed;

import android.content.Context;
import android.widget.LinearLayout;

import org.narcotek.cputemp.common.log.XposedLogger;
import org.narcotek.cputemp.systemui.ui.CpuTempView;

import de.robv.android.xposed.IXposedHookInitPackageResources;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.callbacks.XC_InitPackageResources.InitPackageResourcesParam;
import de.robv.android.xposed.callbacks.XC_LayoutInflated;

/**
 * This class is run after boot. This is where the custom TextView is created in the context of
 * the SystemUI app.
 */
public final class CpuTempModule implements IXposedHookInitPackageResources {

    private static final String SYSUI_PACKAGE = "com.android.systemui";

    @Override
    public void handleInitPackageResources(InitPackageResourcesParam resParam) throws Throwable {
        // Hooking into SystemUI
        if (resParam.packageName.equals(SYSUI_PACKAGE)) {

            // Checking if status_bar_simple exists, meaning the device is running a MIUI ROM.
            if (resParam.res.getIdentifier("status_bar_simple", "layout", SYSUI_PACKAGE) != 0) {
                /*
                 * For MIUI ROMs!
                 *
                 * MIUI uses different layouting than AOSP based ROMs for the status bar. The actual
                 * status bar has the ID status_bar_simple instead of status_bar (NOTE: This is MIUI 7).
                 */
                resParam.res.hookLayout(SYSUI_PACKAGE, "layout", "status_bar_simple", new XC_LayoutInflated() {

                    @Override
                    public void handleLayoutInflated(LayoutInflatedParam liParam) throws Throwable {
                        XposedLogger logger = XposedLogger.getInstance();

                        try {
                            Context context = liParam.view.getContext();

                            logger.log("MIUI status bar inflated (status_bar_simple).");

                            /*
                             * notification_icon_area:
                             * > Contains notification icons
                             * > Is a LinearLayout in MIUI 7
                             * > Only visible if active in MIUI settings
                             *
                             * statusbar_icon:
                             * > Contains the system icons (network, bluetooth, ...)
                             * > Is a StatusBarIcons instance (inherits from LinearLayout)
                             */
                            LinearLayout notificationIconArea = (LinearLayout) liParam.view.findViewById(
                                    liParam.res.getIdentifier("notification_icon_area", "id", SYSUI_PACKAGE));

                            LinearLayout systemIconArea = (LinearLayout) liParam.view.findViewById(
                                    liParam.res.getIdentifier("statusbar_icon", "id", SYSUI_PACKAGE));

                            // Attaching custom view on boot; BootReceiver initializes the label
                            new CpuTempView(context, notificationIconArea, systemIconArea);
                            logger.log("MIUI status bar modified; created and inserted label.");
                        } catch (Exception ex) {
                            logger.log("Modifying MIUI status bar failed.\n" + ex.getMessage());
                        }
                    }

                });
            } else if (resParam.res.getIdentifier("status_bar", "layout", SYSUI_PACKAGE) != 0) {
                /*
                 * For AOSP based ROMs!
                 *
                 * AOSP based ROMs have the status bar ID status_bar.
                 */
                resParam.res.hookLayout(SYSUI_PACKAGE, "layout", "status_bar", new XC_LayoutInflated() {

                    @Override
                    public void handleLayoutInflated(LayoutInflatedParam liParam) throws Throwable {
                        XposedLogger logger = XposedLogger.getInstance();

                        try {
                            Context context = liParam.view.getContext();

                            logger.log("AOSP status bar inflated (status_bar).");

                            /*
                             * notification_icon_area_inner:
                             * > Contains notification icons
                             * > Is a LinearLayout in 5.0+
                             *
                             * system_icon_area:
                             * > Contains the system icons (network, bluetooth, ...)
                             * > Is a AlphaOptimizedLinearLayout in 5.0+
                             */
                            // Retrieving the status bar content area by getting the view ID
                            LinearLayout notificationIconArea = (LinearLayout) liParam.view.findViewById(
                                    liParam.res.getIdentifier("notification_icon_area_inner", "id", SYSUI_PACKAGE));

                            // Retrieving the system icon area the same way
                            LinearLayout systemIconArea = (LinearLayout) liParam.view.findViewById(
                                    liParam.res.getIdentifier("system_icon_area", "id", SYSUI_PACKAGE));

                            // Attaching custom view on boot; BootReceiver initializes the label
                            new CpuTempView(context, notificationIconArea, systemIconArea);
                            logger.log("AOSP status bar modified; created and inserted label.");
                        } catch (Exception ex) {
                            logger.log("Modifying AOSP status bar failed.\n" + ex.getMessage());
                        }
                    }

                });
            } else {
                XposedBridge.log("ROM is not supported (yet); status bar not modified.");
            }
        }
    }

}
