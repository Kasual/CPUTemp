package org.narcotek.cputemp.systemui.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.narcotek.cputemp.common.helper.IntentStrings;
import org.narcotek.cputemp.common.log.DummyLogger;
import org.narcotek.cputemp.common.log.Logger;
import org.narcotek.cputemp.common.log.XposedLogger;
import org.narcotek.cputemp.systemui.helper.StringUtils;
import org.narcotek.cputemp.systemui.io.DefaultTempReader;
import org.narcotek.cputemp.systemui.io.RootTempReader;
import org.narcotek.cputemp.systemui.io.TempReader;
import org.narcotek.cputemp.systemui.root.RootNotGrantedException;

import java.io.File;
import java.util.HashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * A modified TextView which is inserted to the status bar to display the temperature. An instance
 * is created at boot in the context of the SystemUI. Therefore it has no access to the ressources
 * (strings, preferences, ...) of the main app is not possible. A thread pool is used to
 * periodically update the label by reading a temperature file. This thread pool is managed by the
 * SystemUI application meaning effectively this part of the code is used only by the SystemUI
 * and Xposed. When the screen is turned off, updating is stopped and started again when the screen
 * is turned on.
 */
public final class CpuTempView extends TextView {

    // Container object for the TextView
    private final LinearLayout container;
    private final int padVal;

    // References to LinearLayout for positioning
    private final LinearLayout leftArea;
    private final LinearLayout rightArea;

    // Preference dependent values
    private boolean enabled;
    private String tempFilePath;
    private int updateInterval; // Interval in which the file is read periodically
    private Position position;
    private int fontSize;
    private int fontColor;
    private Colorizer colorizer;
    private String formatString; // A format string for temperature
    private int precision;
    private double divisor;
    private boolean useFahrenheit;
    private Logger logger; // Logs only when activated

    // Temperature file reference
    private File tempFile;

    // Used for communication with the preference app and receiving screen broadcasts
    private final BroadcastReceiver cpuTempBroadcastReceiver;
    private final IntentFilter filter;

    // Objects for periodic updating
    private final ScheduledThreadPoolExecutor exec;
    private ScheduledFuture future;
    private final Runnable updateRoutine;
    private boolean updating;
    private TempReader tempReader;

    /**
     * Default constructor
     *
     * @param context   Context of the SystemUI applcation
     * @param leftArea  Reference to a LinearLayout on the left side of the status bar
     * @param rightArea Reference to a LinearLayout on the right side
     */
    public CpuTempView(Context context, LinearLayout leftArea, LinearLayout rightArea) {
        super(context);

        if (leftArea == null || rightArea == null) {
            throw new NullPointerException("CpuTempView arguments (left and right area references) must not be null!");
        }

        /*
         * Usually the leftArea field is the notification area or the status bar itself. The
         * rightArea is usually the system icon area containing the network status icon, the
         * bluetooth icon, and so on.
         */
        // Setting the references
        this.leftArea = leftArea;
        this.rightArea = rightArea;

        // Logging is active during startup
        logger = XposedLogger.getInstance();

        // Container for CPU temp label; easier to format this way
        container = new LinearLayout(context);
        container.setOrientation(LinearLayout.HORIZONTAL);
        container.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT));
        container.setGravity(Gravity.CENTER_VERTICAL);
        container.addView(this);

        this.setSingleLine(true);

        // 6 dp to X px
        padVal = (int) (6 * getResources().getDisplayMetrics().density + 0.5f);

        // Handles screen events and preference updates
        cpuTempBroadcastReceiver = new CpuTempBroadcastReceiver();

        // Update routine is later passed the thread pool
        updateRoutine = new UpdateRoutineRunnable();

        // ThreadPoolExecutor initialization
        exec = new ScheduledThreadPoolExecutor(1);

        // Registering receiver
        filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(IntentStrings.INTENT_CPUTEMP_PREF_UPDATE);
        this.getContext().registerReceiver(cpuTempBroadcastReceiver, filter);
    }

    /**
     * Sets the preference fields with the values of the HashMap.
     *
     * @param prefs HashMap of preferences
     * @throws Exception if preferences are corrupt
     */
    private void setPreferences(HashMap prefs) throws Exception {
        /*
         * String resources can't be accessed from here, because the TextView is created in the
         * context of the SystemUI app.
         */
        enabled = Boolean.parseBoolean(prefs.get("enable_ct").toString());

        tempFilePath = prefs.get("temp_file").toString();
        tempFile = new File(tempFilePath);

        updateInterval = Integer.valueOf(prefs.get("update_interval").toString());

        fontSize = Integer.valueOf(prefs.get("font_size").toString());
        fontColor = Integer.parseInt(prefs.get("font_color").toString());

        if (((Boolean) prefs.get("enable_limit")).booleanValue()) {
            colorizer = new LimitColorizer(Double.valueOf(prefs.get("temp_limit").toString()),
                    Integer.valueOf(prefs.get("limit_font_color").toString()));
        } else {
            colorizer = new DummyColorizer();
        }

        formatString = prefs.get("format_string").toString();
        divisor = Double.valueOf(prefs.get("divisor").toString());
        useFahrenheit = Boolean.parseBoolean(prefs.get("use_fahrenheit").toString());
        precision = StringUtils.findPrecision(formatString);

        boolean logging = ((Boolean) prefs.get("enable_log")).booleanValue();
        logger = logging ? XposedLogger.getInstance() : DummyLogger.getInstance();

        String pos = prefs.get("position").toString();
        if (pos.equals("0")) {
            position = Position.ABSOLUTE_RIGHT;
        } else if (pos.equals("1")) {
            position = Position.RIGHT;
        } else {
            position = Position.LEFT;
        }

        tempReader = null;
        boolean useRoot = ((Boolean) prefs.get("use_root")).booleanValue();
        tempReader = useRoot ? new RootTempReader() : new DefaultTempReader();
    }

    /**
     * Sets the preference fields to a "safe mode" if preferences are corrupt.
     */
    private void setDebugPreferences() {
        // Default settings, only logging is activated.
        enabled = true;

        tempFilePath = "/sys/class/thermal/thermal_zone0/temp";
        tempFile = new File(tempFilePath);
        tempReader = new DefaultTempReader();

        updateInterval = 1000;

        position = Position.ABSOLUTE_RIGHT;
        fontSize = 12;
        fontColor = Color.WHITE;
        colorizer = new DummyColorizer();

        formatString = "%T%U";
        precision = 0;
        divisor = 1;
        useFahrenheit = false;

        logger = XposedLogger.getInstance();
    }

    /**
     * Detaches the label from the status bar.
     */
    private void detachContainer() {
        leftArea.removeView(container);
        rightArea.removeView(container);

        logger.log("Detached from status bar.");
    }

    /**
     * Sets the label's style and attaches it to the status bar.
     */
    private void attachContainer() {
        this.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
        this.setTextColor(fontColor);

        switch (position) {
            case ABSOLUTE_RIGHT:
                rightArea.addView(container);
                container.setPadding(padVal, 0, 0, 0);
                break;
            case RIGHT:
                container.setPadding(padVal, 0, padVal, 0);
                rightArea.addView(container, 0);
                break;
            case LEFT:
                container.setPadding(0, 0, padVal, 0);
                leftArea.addView(container, 0);
                break;
        }

        logger.log("Attached to status bar.");
    }

    /**
     * Starts the update periodic updates.
     */
    private void startUpdating() {
        if (tempReader != null) {
            future = exec.scheduleAtFixedRate(updateRoutine, 0, updateInterval, TimeUnit.MILLISECONDS);
        }
        updating = true;

        logger.log("Started updating.");
    }

    /**
     * Stops the periodic udpates.
     */
    private void stopUpdating() {
        if (future != null) {
            future.cancel(false);
        }
        updating = false;

        logger.log("Stopped updating.");
    }

    /**
     * Sets the labels text to an 'x' and shows a Toast with the given error message.
     *
     * @param errorMsg Error message to show
     */
    private void showError(String errorMsg) {
        this.setText("×");
        Toast.makeText(this.getContext(), errorMsg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Returns the saved preferences of this label.
     *
     * @return Preferences as readable string
     */
    @Override
    public String toString() {
        return "Configuration -> {" +
                "enabled=" + enabled +
                ", tempFilePath='" + tempFilePath + '\'' +
                ", updateInterval=" + updateInterval +
                ", position=" + position +
                ", fontSize=" + fontSize +
                ", fontColor=" + fontColor +
                ", formatString='" + formatString + '\'' +
                ", precision=" + precision +
                ", divisor=" + divisor +
                ", useFahrenheit=" + useFahrenheit +
                ", logger=" + logger.toString() +
                ", tempReader=" + tempReader.toString() +
                '}';
    }

    /**
     * Inner class for broadcast receiver
     */
    private class CpuTempBroadcastReceiver extends BroadcastReceiver {

        // This flag prevents the TextView from using empty/null preference fields on boot
        boolean bootReceived = false;

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_ON) && bootReceived && !updating) {
                logger.log("Received screen on.");
                startUpdating(); // Screen on -> updating again
            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF) && bootReceived && updating) {
                logger.log("Received screen off.");
                stopUpdating();
            } else if (intent.getAction().equals(IntentStrings.INTENT_CPUTEMP_PREF_UPDATE)) {
                logger.log("Received preferences update.");

                // Stops update routine and detaches the container from the status bar
                stopUpdating();
                detachContainer();

                try {
                    setPreferences((HashMap) intent.getSerializableExtra("prefs"));
                } catch (RootNotGrantedException ex) {
                    logger.log(ex.getMessage());

                    showError("Root was not granted to the SystemUI app!");
                } catch (Exception ex) {
                    logger.log("Error retrieving loading preferences, using debug preferences.\n" + ex.getMessage());
                    setDebugPreferences();

                    showError("Preferences could not be loaded!");
                } finally {
                    logger.log(CpuTempView.this.toString());
                }

                // If enabled the container is attached to the status bar and updatig is started
                if (enabled) {
                    attachContainer();
                    startUpdating();
                }

                bootReceived = true;
            }
        }

    }

    /**
     * Inner class for update routine
     */
    private class UpdateRoutineRunnable implements Runnable {

        @Override
        public void run() {
            try {
                // Reading value from file
                double temp = tempReader.readTemperature(tempFile);
                logger.log("Read value from file: " + temp);

                // Calculation
                temp /= divisor;
                if (useFahrenheit) {
                    temp = temp * 1.8 + 32.0;
                }
                logger.log("Calculated value: " + temp);

                // Setting new text
                final String newTxt = formatString.replaceFirst("%\\d*T", String.format("%." + precision + "f", temp))
                        .replaceFirst("%U", useFahrenheit ? "°F" : "°C");

                final double tempCopy = temp;

                // Updating UI from within the UI thread
                CpuTempView.this.post(new Runnable() {

                    @Override
                    public void run() {
                        colorizer.colorize(tempCopy);
                        CpuTempView.this.setText(newTxt);
                    }

                });
            } catch (Exception e) {
                logger.log(e.getMessage());

                CpuTempView.this.post(new Runnable() {

                    @Override
                    public void run() {
                        colorizer.colorize(0);
                        CpuTempView.this.setText("×");
                        future.cancel(false);
                    }

                });
            }
        }

    }

    /**
     * Implementation of the Colorizer interface which colors when the given temperature limit is
     * exceeded.
     */
    private class LimitColorizer implements Colorizer {

        private double limit;
        private int fontColor;

        public LimitColorizer(double limit, int fontColor) {
            this.limit = limit;
            this.fontColor = fontColor;
        }

        @Override
        public void colorize(double temp) {
            if (temp >= limit) {
                CpuTempView.this.setTextColor(fontColor);
            } else {
                CpuTempView.this.setTextColor(CpuTempView.this.fontColor);
            }
        }

        @Override
        public String toString() {
            return "LimitColorizer{" +
                    "limit=" + limit +
                    ", fontColor=" + fontColor +
                    '}';
        }

    }

    /**
     * DummyColorizer which doesn't color.
     */
    private class DummyColorizer implements Colorizer {

        @Override
        public void colorize(double temp) {
            // Nothing to do
        }

        @Override
        public String toString() {
            return "DummyColorizer";
        }

    }

}
