package org.narcotek.cputemp.systemui.io;

import org.narcotek.cputemp.systemui.root.RootNotGrantedException;
import org.narcotek.cputemp.systemui.root.Shell;

import java.io.File;
import java.io.IOException;

/**
 * A temperature reader using Java's Process API by gaining root access and using the "cat" command.
 */
public class RootTempReader implements TempReader {

    private Shell shell;

    public RootTempReader() throws IOException, RootNotGrantedException {
        shell = new Shell(true);
    }

    @Override
    public double readTemperature(File tempFile) throws Exception {
        String temp = shell.run("cat " + tempFile.getPath());

        double out = Double.parseDouble(temp);

        return out;
    }

    @Override
    public String toString() {
        return "RootTempReader";
    }
}
