package org.narcotek.cputemp.systemui.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Default temperature reader using Java's IO API.
 */
public class DefaultTempReader implements TempReader {

    @Override
    public double readTemperature(File tempFile) throws FileNotFoundException {
        double out;

        Scanner scanner = new Scanner(tempFile);

        out = scanner.nextDouble();

        scanner.close();

        return out;
    }

    @Override
    public String toString() {
        return "DefaultTempReader";
    }
}
