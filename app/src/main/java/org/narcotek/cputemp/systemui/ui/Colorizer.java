package org.narcotek.cputemp.systemui.ui;

/**
 * An interface which is used to colorize the temperature label in specific situations.
 */
public interface Colorizer {

    void colorize(double temp);

}
