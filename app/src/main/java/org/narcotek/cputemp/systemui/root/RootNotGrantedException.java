package org.narcotek.cputemp.systemui.root;

/**
 * Thrown by the constructor of the Shell class if root access was denied.
 */
public class RootNotGrantedException extends Exception {

    public RootNotGrantedException(String message) {
        super(message);
    }

}
