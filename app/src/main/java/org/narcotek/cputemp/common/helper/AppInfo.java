package org.narcotek.cputemp.common.helper;

/**
 * Small helper class containing basic application info
 */
public final class AppInfo {

    public static final String CPUTEMP_PACKAGE = "org.narcotek.cputemp";

}
