package org.narcotek.cputemp.common.log;

/**
 * Logger interface which allows for optional logging.
 */
public interface Logger {

    void log(String message);

}
