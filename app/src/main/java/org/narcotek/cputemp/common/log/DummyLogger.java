package org.narcotek.cputemp.common.log;

/**
 * A dummy logger, which doesn't log.
 */
public final class DummyLogger implements Logger {

    private DummyLogger() {
    }

    public static DummyLogger getInstance() {
        return Holder.INSTANCE;
    }

    @Override
    public void log(String message) {
    }

    private static class Holder {
        static final DummyLogger INSTANCE = new DummyLogger();
    }

    @Override
    public String toString() {
        return "DummyLogger";
    }

}
