package org.narcotek.cputemp.common.log;

import org.narcotek.cputemp.common.helper.AppInfo;

import de.robv.android.xposed.XposedBridge;

/**
 * Logger which logs with the XposedBridge.
 */
public final class XposedLogger implements Logger {

    private XposedLogger() {
    }

    public static XposedLogger getInstance() {
        return Holder.INSTANCE;
    }

    @Override
    public void log(String message) {
        XposedBridge.log(String.format("%s: %s", AppInfo.CPUTEMP_PACKAGE, message));
    }

    private static class Holder {
        static final XposedLogger INSTANCE = new XposedLogger();
    }

    @Override
    public String toString() {
        return "XposedLogger";
    }

}
