# CPUTemp
CPUTemp is a Xposed module which adds a label to the status bar showing the current CPU temperature of your device.

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.me/narcotek)

## About
Linux-based systems allow applications and processes to read sensor values by providing in-memory files containing the sensor's values. Among other sensor values the CPU temperature can be retrieved this way.

The CPUTemp application uses this technique to read a thermometer file containing the CPU temperature periodically and update a label to display the temperature. This label is inserted at boot into the Android device's status bar using Xposed.

## Requirements
* Xposed Version 81+
* Android 5.0+

For older versions you may want to use [this app](http://forum.xda-developers.com/xposed/modules/mod-cputemp-statusbar-t2494170) by XDA user m11kkaa.

## Installation
Install application (from the Xposed repo [here](http://repo.xposed.info/module/org.narcotek.cputemp)) and activate the module. You should change the default file path before rebooting.

The temperature file for your CPU thermometer should be in a subdirectory of "/sys/". However the temperature file name and path is device dependent. This means you need to search this directory for the correct file and subdirectory. Moreover not every device has a specific CPU thermometer but several other thermometers distributed on the devices board. The one nearest to the CPU has the "most correct" measured temperature and their subdirectory usually has a trailing zero in the name (e. g. "thermal_zone0") or is named something like "core_temp". The default file path ("/sys/class/thermal/thermal_zone0/temp") therefore won't work on every device.

If the temperature is not shown in the status bar, but the status bar icons are moved or an 'x' is shown, the app probably has problems reading the temperature file. Reasons are the format of the temperature file or missing permissions. For the latter you can try to enable the "use root" setting in the CPUTemp app, which causes the SystemUI app (which effectively is reading the temperature file) to ask for root permissions. This way it should be garantueed that missing permissions are not the reason for the module not to work.

## Permissions
The "run at startup" permission is used to set the saved preferences for the label.

The SystemUI asks for root priviliges **only** if the "use root" option is checked. This is a fix for some ROMs/devices where permission to read the temperature file gets denied.

## Bug reporting
If you found a bug in this software feel free to open an issue. Please watch out for duplicates. **Important:** Turn on logging in the app and restart your phone. Wait a few minutes and attach the Xposed log to your issue post. Also mention your ROM, Android version and device in the post.

## Libraries
CPUTemp uses the [ColorPickerView](https://github.com/danielnilsson9/color-picker-view) library by danielnilsson9 and the [XposedBridge](https://github.com/rovo89/XposedBridge/) library by rovo89. ColorPickerView and XposedBridge are licensed under the **Apache License 2.0**. A copy of this license can be found on this repository ([THIRDPARTY.TXT](THIRDPARTY.TXT)).

## License
CPUTemp is licensed under the **GNU General Public License Version 3.0 (GNU-GPLv3)**. A copy of the license can be found on this repository ([LICENSE.TXT](LICENSE.TXT)).

    Copyright (C) 2016  narcotek

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
